<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml"
	omit-xml-declaration="yes"
	encoding="UTF-8"
	indent="yes" />

<xsl:strip-space elements="*"/>
<!--
<xsl:template match="image" priority="-1">
  <xsl:element name="{name()}"> - 
    <xsl:apply-templates select="* | @* | text()" />
  </xsl:element>
</xsl:template>
-->
<xsl:template match='/'>
  <xsl:apply-templates match="*" />
</xsl:template>

<xsl:template match="*">
  <xsl:if test='descendant::image or normalize-space(.) or name() = "image"'>
    <xsl:element name="{name()}">
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:if>
</xsl:template>

<xsl:template match="@*">
   <xsl:attribute name="{name(.)}">
       <xsl:value-of select="."/>
   </xsl:attribute>
</xsl:template>

</xsl:stylesheet>
