<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml"
	omit-xml-declaration="yes"
	encoding="UTF-8"
	indent="yes" />

<xsl:strip-space elements="*"/>

<xsl:param name='section' />

<xsl:template match='/'>
  <xsl:choose>
    <xsl:when test='$section = "all"'>
      <div class='references'>
        <xsl:call-template name='all-refs' />
      </div>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name='references'>
        <xsl:with-param name='section' select='$section' />
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name='all-refs'>
  <xsl:for-each select="//h1">
    <xsl:if test='name() = "h1"'>
      <xsl:apply-templates select="following-sibling::*[1]" />
    </xsl:if>
  </xsl:for-each>
  <xsl:text> </xsl:text>
</xsl:template>

<xsl:template name='references'>
  <xsl:param name='section' select='"1"' />
  <div class='references'>
  <xsl:for-each select="//h1">
    <xsl:if test='name() = "h1" and position() = $section'>
      <xsl:apply-templates select="following-sibling::*[1]" />
    </xsl:if>
  </xsl:for-each>
  <xsl:text> </xsl:text>
  </div>
</xsl:template>

<!-- This is specific to HRW's implementation -->
<xsl:template match='*'>
  <xsl:if test='name() != "h1"'>
    <xsl:if test='name() = "a" and substring(@href, 1, 1) = "#"'>
      <xsl:call-template name='reference' select='.' />
    </xsl:if>
    <xsl:if test='name() != "a"'>
      <xsl:call-template name='reference' select="./*" />
    </xsl:if>
    <xsl:apply-templates select="following-sibling::*[1]" />  
  </xsl:if>
</xsl:template>

<xsl:template name='reference'>
  <xsl:choose>
    <xsl:when test='name() = "a" and substring(@href, 1, 1) = "#"'>
      <xsl:variable name="id" select="substring-after(@href, '#_')" />
      <xsl:copy-of select="//*[@id = $id]" />
    </xsl:when>
    <xsl:otherwise>
      <xsl:for-each select="./*">
        <xsl:call-template name='reference' select="./*" />
      </xsl:for-each>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>