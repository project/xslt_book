<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml"
	omit-xml-declaration="yes"
	encoding="UTF-8"
	indent="yes" />

<xsl:strip-space elements="*"/>

<xsl:param name='section' />

<xsl:template match='/'>
  <xsl:choose>
    <xsl:when test='$section = "all"'>
      <div>
      <xsl:call-template name='all-sections' />
      </div>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name='get-section'>
        <xsl:with-param name='section' select='$section' />
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name='all-sections'>
  <xsl:for-each select="//h1">
    <div id='xslt-book-section-{position()}' class='section section-{position()}'>
      <xsl:if test='name() = "h1"'>
        <xsl:copy-of select="." />
        <xsl:apply-templates select="following-sibling::*[1]" />
      </xsl:if>
    </div>
  </xsl:for-each>
</xsl:template>

<xsl:template name='get-section'>
  <xsl:param name='section' select='"1"' />
  <div class='section section-{$section}'>
  <xsl:for-each select="//h1">
    <xsl:if test='name() = "h1" and position() = $section'>
      <xsl:copy-of select="." />
      <xsl:apply-templates select="following-sibling::*[1]" />
    </xsl:if>
  </xsl:for-each>
  </div>
</xsl:template>

<xsl:template match='*'>
  <xsl:if test='name() != "h1"'>
    <xsl:copy-of select="." />
    <xsl:apply-templates select="following-sibling::*[1]" />  
  </xsl:if>
</xsl:template>

<xsl:template match="image">
  <xsl:element name="{name()}">
    <xsl:apply-templates select="* | @* | text()" />
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
