<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml"
	omit-xml-declaration="no"
	encoding="UTF-8"
	indent="yes" />

<xsl:strip-space elements="*"/>

<xsl:param name='node_url' />
<xsl:param name='depth' select='"1"' />
<xsl:param name='section' />

<xsl:template match='/'>
  <xsl:call-template name='get-tree' />
</xsl:template>

<xsl:template name='get-tree'>
  <ul class='xslt-book'>
  <xsl:for-each select='//h1'>
    <li>
    <xsl:choose>
      <xsl:when test='$section = "all"'>
        <a href='#xslt-book-section-{position()}'><xsl:value-of select="." /></a>
      </xsl:when>
      <xsl:otherwise>
        <a href='{$node_url}/section/{position()}'><xsl:value-of select="." /></a>
      </xsl:otherwise>
    </xsl:choose>
      <ul>
      <xsl:apply-templates select="following-sibling::*[1]">
        <xsl:with-param name='tag' select='2' />
        <xsl:with-param name='stop' select='name()' />
      </xsl:apply-templates> 
      </ul>
    </li>
  </xsl:for-each>
  </ul>
</xsl:template>

<xsl:template match='*'>
  <xsl:param name='tag'/>
  <xsl:param name='stop'/>
  <xsl:if test='$tag &lt;= $depth'>
  <xsl:choose>
    <xsl:when test='name() = $stop'></xsl:when>
    <xsl:when test='name() = concat("h", $tag)'>    
      <li>
        <!--<xsl:apply-templates />-->
        <xsl:value-of select='.' />
        <ul>
        <xsl:apply-templates select="following-sibling::*[1]">
          <xsl:with-param name='tag' select='$tag + 1' />
          <xsl:with-param name='stop' select='name()' />
        </xsl:apply-templates>
        </ul>
      </li>
      <xsl:apply-templates select="following-sibling::*[1]">
        <xsl:with-param name='tag' select='$tag' />
        <xsl:with-param name='stop' select='$stop' />
      </xsl:apply-templates>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="following-sibling::*[1]">
        <xsl:with-param name='tag' select='$tag' />
        <xsl:with-param name='stop' select='$stop' />
      </xsl:apply-templates>
    </xsl:otherwise>
  </xsl:choose>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>